import { Controller, Body, Post } from '@nestjs/common';
import { AuthLoginReq } from 'types';

@Controller()
export class AppController {
  @Post()
  getTestItfForm(@Body() body: AuthLoginReq): void {
    console.log('Success', body);
  }
}
