import { PipeTransform, Injectable, ArgumentMetadata, BadRequestException, Type } from '@nestjs/common';
import { FormValidation, ApiErrorCode } from 'types';

@Injectable()
export class ValidationPipe implements PipeTransform<any> {
  transform = async (value: any, { metatype }: ArgumentMetadata) => {
    if (!metatype || !this.toValidate(metatype)) {
      return value;
    }
    return await this.valid(value, metatype);
  }

  private toValidate = (metatype: Function): boolean => { // tslint:disable-line
    const types: Function[] = [String, Boolean, Number, Array, Object]; // tslint:disable-line
    return !types.includes(metatype);
  }

  private valid = async (value: any, metatype: Type<any>) => {
    const schema = FormValidation.getSchema(metatype);
    if (!schema) {
      return value;
    }
    return await schema.validate(value, { stripUnknown: true })
    .catch(err => {
      throw new BadRequestException(ApiErrorCode.InvalidFormData);
    })
  }
}